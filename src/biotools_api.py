import sys
import json
import requests
import argparse
import yaml
import pandas as pd

# The call to get all bio.tools entries through the api is: https://bio.tools/api/tool/?format=json.
# Unfortunately, the response is limited to n tools. To get the whole set, the next pages must be retrieve.
# use "next" in the response to get succesive entries.
 
base_call = "https://bio.tools/api/tool/?format=json"


def make_request(URL):
    try:
        response = requests.get(URL)
    except:
        print('Could not make the request')
        return
    else:
        response = json.loads(response.text)
        return(response)

def build_url(next_page, filters):
    call_template = "https://bio.tools/api/tool/?{description}{topic}{topic_id}{next_page}&format=json"
    if next_page:
        next_page = "&%s"%next_page
    else:
        next_page = ""
    desc = 'description="%s"'%(filters.get("description"))
    topic =  '&topic="%s"'%(filters.get("topic"))
    topic_id = "&topicID=%s"%(filters.get("topic_id"))
    
    url = call_template.format(next_page=next_page, description=desc, topic=topic, topic_id=topic_id)
    return(url)


def get_all_pages(filters):
    res = []
    next_page = "page=1"
    print('Starting making the requests...')
    while next_page:
        response = make_request(build_url(next_page, filters))
        print("Requesting: " + build_url(next_page, filters), end='\r')
        res = res + response["list"]
        next_page = response["next"]
        if next_page:
            next_page = next_page[1:]
    print('\nRequests finished.')
    return(res)

def save_result(out_name, result):
    with open(out_name, 'w') as out:
        json.dump(result, out)
    print('Result saved as ' + out_name )

def get_config(args):
    config_file = args[1]
    with open(config_file, 'r') as stream:
        try:
            config = yaml.safe_load(stream)
            return(config)
        except yaml.YAMLError as exc:
            print(exc)


import biotools_parse as bp 

if __name__ == '__main__':
    
    config = get_config(sys.argv)
    filters = config['filters']
    print(filters)
    result = get_all_pages(filters)
    tools = bp.biotoolsToolsGenerator(result).instances
    
    # Load features into table
    colnames_features = [ 'name', 'description', 'version', 'type', 'topic', 'links', 'publication', 'download', 'inst_instr', 'test', 'src', 'os', 'input', 'output', 'dependencies', 'documentation', 'license', 'termsUse', 'contribPolicy', 'authors', 'repository']
    df_dict = dict()
    for name in colnames_features:
        df_dict[name] = []
        
    for tool in tools:
        for field in colnames_features:
            df_dict[field].append(tool.__dict__.get(field))
       
    df_feaures = pd.DataFrame.from_dict(df_dict)

    df_feaures.to_csv(config['output_path']+'/'+config['output_name']+'.csv', index=False)

    print(result[1].keys())
    #save_result(config['json_output_path'], result)