Application Name:	ZOOMA (Automatic Ontology Mapper)
Version:	2.0
Run at:	13:36.39, 17.12.20
Run from:	http://www.ebi.ac.uk/fgpt/zooma


PROPERTY TYPE	PROPERTY VALUE	ONTOLOGY TERM LABEL(S)	ONTOLOGY TERM SYNONYM(S)	CONFIDENCE	ONTOLOGY TERM(S)	ONTOLOGY(S)	SOURCE(S)	STUDY
[NO TYPE]	Ontology annotation, 	Annotation		Medium	operation_0226	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Ontology annotation, 	Ontology visualisation		Medium	operation_3559	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	semantic annotation	Annotation		Medium	operation_0226	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	semantic annotation	Text annotation		Medium	operation_3778	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Text mining	Natural language processing		Good	topic_0218	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Entity tagging,	ConsensusPathDB entity ID		Good	data_2776	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Entity recognition,	Data mining		Good	topic_3473	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Query expansion	Query language		Medium	format_3787	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Query expansion	XQuery		Medium	format_3789	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Knowledge graph	Plot		Medium	data_2884	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Knowledge graph	FASTG		Medium	format_3823	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Module extraction (eg slims)	Information extraction		Medium	operation_3907	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Module extraction (eg slims)	Feature extraction		Medium	operation_3937	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Free text mapping,	Text		Good	data_3671	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	Machine learning, 	Machine learning		Good	topic_3474	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	NLP (eg TensorFlow, BioBert)	NLP format		Medium	format_3841	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
[NO TYPE]	NLP (eg TensorFlow, BioBert)	NLP corpus format		Medium	format_3863	http://edamontology.org/	http://edamontology.org	[UNKNOWN EXPERIMENTS]
