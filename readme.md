## Usage

```
python3 biotools_api.py config.yaml
```
### Config file
The configuration file stated the filters applied in the call and the path where the response json will be saved.
Example of configuration file:

```
filters:
 topic_id: ""
 description: "game"
 topic: "Sequence composition, complexity and repeats"
json_output_path: ../output/run_3.json
```
Warning: quotation marks should be preserved for the filters even when the field is empty.


## Requirements
The libraries `pyYAML` and `requests` are needed to run this program. 
A virtual environment can be used. To do so, run:

```
pipenv install
```
Then, do not forget to run the program in that environment either from `pipenv shell` or using `pipenv run` before each command.

Alternaively to using a virtual environment, you can install the requirements in your system:
```
python3 -m pip install pyYAML
```
```
python3 -m pip install requests
```